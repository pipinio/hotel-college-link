<?php
require_once __DIR__ . "/includes/_session.php";
if (!$user_id) {
    header('Location: /');
    exit();
}
$user = new User();
$user_details = $user->getUserDetails();

//print '<pre>' . print_r($user_details) . '</pre>';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hotels - Προφίλ Χρήστη</title>
    <meta name="description" content="Hotel College link project">
    <meta name="author" content="Despina Litsa">
    <?php
    include_once __DIR__ . '/includes/_css.php';
    ?>
</head>
<body>
<?php
include_once __DIR__ . '/includes/_header.php';
?>
<div class="wrapper">
    <div class="container container_with_sidebar">
        <div class="sidebar">
            <div class="favorites_container">
                <div class="title">FAVORITES</div>
                <div class="favorites_list">
                    <?php
                    if (count($user_details['favorites'])) {
                        ?>
                        <ol>
                            <?php
                            foreach ($user_details['favorites'] as $favorite) {
                                ?>
                                <li><?= $favorite['name'] ?></li>
                                <?php
                            }
                            ?>
                        </ol>
                    <?php } else {
                        print 'No favorites.';
                    } ?>
                </div>
            </div>
            <div class="reviews_container">
                <div class="title">REVIEWS</div>
                <div class="reviews_list">
                    <?php
                    if (count($user_details['reviews'])) {
                        ?>
                        <ol>
                            <?php
                            foreach ($user_details['reviews'] as $review) {
                                ?>
                                <li>
                                    <div class="list_title"><?= $review['name'] ?></div>
                                    <div class="stars_container"><?= $review['stars_html'] ?></div>
                                </li>
                            <?php } ?>
                        </ol>
                    <?php } else {
                        print 'No reviews.';
                    } ?>
                </div>
            </div>
        </div>
        <div class="list_container">
            <div class="my_bookings_title">My bookings</div>
            <div class="column_list bookings_list">
                <?php
                if (count($user_details['bookings'])) {
                    foreach ($user_details['bookings'] as $booking) {
                        ?>
                        <div class="row_item booking_item">
                            <div class="left_part">
                                <div class="item_photo"
                                     style="background-image: url('../images/rooms/<?= $booking['photo_url'] ?>');">
                                </div>
                                <div class="price_container total_cost">Total Cost: <?= $booking['total_price'] ?>€
                                </div>
                            </div>
                            <div class="right_part">
                                <div class="item_info">
                                    <div class="info_parts hotel_title"><?= $booking['name'] ?></div>
                                    <div class="info_parts hotel_location"><?= $booking['city'] ?>
                                        , <?= $booking['area'] ?></div>
                                    <div class="info_parts hotel_description"><?= $booking['description_short'] ?></div>
                                    <div class="hotel_button_link"><a href="/room.php?id=<?= $booking['room_id'] ?>">Go
                                            to Room Page</a></div>
                                </div>
                                <div class="all_booking_details">
                                    <div class="all_booking_detail_item">Check-in
                                        Date: <?= $booking['check_in_date'] ?></div>
                                    <div class="all_booking_detail_item">Check-out
                                        Date: <?= $booking['check_out_date'] ?></div>
                                    <div class="all_booking_detail_item">Type of
                                        Room: <?= $booking['room_type_title'] ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    print 'No bookings.';
                } ?>
            </div>
        </div>
    </div>
</div>
<?php
include_once __DIR__ . '/includes/_footer.php';
include_once __DIR__ . '/includes/_js.php';
?>
</body>
</html>