$(function () {
    /**
     * Initialie datepicker elements
     */
    $('.datepicker_element').each(function () {
        let _this = $(this);
        _this.datepicker({dateFormat: 'yy-mm-dd'});
    });

    /**
     * Initialize range slider elements
     */
    $('.range_slider_element').each(function () {
        let _this = $(this);
        let closest_form = _this.closest('form');
        let id = _this.attr('id');

        let min_value = _this.data('min_value');
        let max_value = _this.data('max_value');

        let selected_min_value = _this.data('selected_min_value');
        let selected_max_value = _this.data('selected_max_value');
        _this.slider({
            min: min_value,
            max: max_value,
            step: 1,
            values: [selected_min_value, selected_max_value],
            slide: function (event, ui) {
                for (var i = 0; i < ui.values.length; ++i) {
                    closest_form.find(".range_slider_inputs_container[data-slider_element='" + id + "'] " +
                        "input.range_slider_inputs[data-index=" + i + "]").val(ui.values[i] + '€');
                }
            }
        });
    });

    $('body').on('keyup', ':input', function (e) {
        let _this = $(this);
        let form = _this.closest('form');
        CoreFunctions.clearErrorsFromForm(form);

        if (e.keyCode === 13) {
            form.find('.generic_submit_button').trigger('click');
        }
    });

    /**
     * Login user
     */
    $('form.login_form').on('click', '.login_button_form', function () {
        let _this = $(this);
        let form = _this.closest('form');
        let form_is_valid = CoreFunctions.validateForm(form);

        if (form_is_valid) {
            CoreFunctions.generateCSRFToken(form).then(function () {
                let post_data = {
                    action: 'loginUser',
                    login_email: form.find(':input[name="login_email"]').val().trim(),
                    login_password: form.find(':input[name="login_password"]').val().trim(),
                    csrfToken: form.find(':input[name="csrfToken"]').val().trim(),
                }
                $.post('requests/core.php', post_data, function (response) {
                    //console.log(response);
                    if (response.hasOwnProperty('OK') && response.OK) {
                        window.location.reload();
                    } else if (response.hasOwnProperty('error') && response.error) {
                        CoreFunctions.handleFormResponseErrors(form, response);
                    } else if (response.hasOwnProperty('auth_error')) {
                        window.location.href = "/logout.php"
                    }
                }, 'json');
            });
        }
    });

    /**
     * Register
     */
    $('form.register_form').on('click', '.register_button_form', function () {
        let _this = $(this);
        let form = _this.closest('form');
        let form_is_valid = CoreFunctions.validateForm(form);

        if (form_is_valid) {
            CoreFunctions.generateCSRFToken(form).then(function () {
                let post_data = {
                    action: 'registerUser',
                    register_name: form.find(':input[name="register_name"]').val().trim(),
                    register_email: form.find(':input[name="register_email"]').val().trim(),
                    register_email_retype: form.find(':input[name="register_email_retype"]').val().trim(),
                    register_password: form.find(':input[name="register_password"]').val().trim(),
                    csrfToken: form.find(':input[name="csrfToken"]').val().trim(),
                }
                $.post('requests/core.php', post_data, function (response) {
                    //console.log(response);
                    if (response.hasOwnProperty('OK') && response.OK) {
                        alert('Success! Your register is complete!');
                        window.location.href = '/login.php';
                    } else if (response.hasOwnProperty('error') && response.error) {
                        CoreFunctions.handleFormResponseErrors(form, response);
                    } else if (response.hasOwnProperty('auth_error')) {
                        window.location.href = "/logout.php"
                    }
                }, 'json');
            });
        }
    });

    /**
     * Index
     * Search form
     */
    $('.search_index_form').on('click', '.search_button', function () {
        let _this = $(this);
        let form = _this.closest('form');
        let form_is_valid = CoreFunctions.validateForm(form);

        if (form_is_valid) {
            let url_parameters = [];

            let city = form.find(':input[name="city"]').val();
            let room_type = form.find(':input[name="room_type"]').val();
            let check_in_date = form.find(':input[name="check_in_date"]').val();
            let check_out_date = form.find(':input[name="check_out_date"]').val();

            if (city) {
                url_parameters.push('city=' + city);
            }
            if (room_type) {
                url_parameters.push('room_type=' + room_type);
            }
            if (check_in_date) {
                url_parameters.push('check_in_date=' + check_in_date);
            }
            if (check_out_date) {
                url_parameters.push('check_out_date=' + check_out_date);
            }

            if (url_parameters.length) {
                window.location.href = '/list.php?' + url_parameters.join('&');
            }
        }
    });

    /**
     * Hover on review stars
     */
    $('.review_stars').on('mouseover', '.review_star', function () {
        let _this = $(this);
        let review_stars_container = _this.closest('.review_stars');
        let index = review_stars_container.find('i').index(_this);
        review_stars_container.find('.review_star').removeClass('active-star');
        for (let i = 0; i <= index; i++) {
            review_stars_container.find('.review_star').eq(i).addClass('active-star');
        }
    });

    $('form.add_review_form').on('click', '.submit_review', function () {
        let _this = $(this);
        let form = _this.closest('form');
        let form_is_valid = CoreFunctions.validateForm(form);

        if (form_is_valid) {
            CoreFunctions.generateCSRFToken(form).then(function () {
                let post_data = {
                    action: 'addReview',
                    my_review: form.find(':input[name="my_review"]').val().trim(),
                    room_id: form.find(':input[name="room_id"]').val().trim(),
                    stars: form.find('.review_star.active-star').length + '',
                    csrfToken: form.find(':input[name="csrfToken"]').val().trim(),
                }
                $.post('requests/core.php', post_data, function (response) {
                    //console.log(response);
                    if (response.hasOwnProperty('OK') && response.OK) {
                        alert('Success! Your review is saved!');
                        window.location.reload();
                    } else if (response.hasOwnProperty('error') && response.error) {
                        CoreFunctions.handleFormResponseErrors(form, response);
                    } else if (response.hasOwnProperty('auth_error')) {
                        window.location.href = "/logout.php"
                    }
                }, 'json');
            });
        }
    });

    $('form.booked_container_form').on('click', '.book_room', function () {
        let _this = $(this);
        let form = _this.closest('form');
        let form_is_valid = CoreFunctions.validateForm(form);

        if (form_is_valid) {
            CoreFunctions.generateCSRFToken(form).then(function () {
                let post_data = {
                    action: 'bookRoom',
                    room_id: form.find(':input[name="room_id"]').val().trim(),
                    check_in_date: form.find(':input[name="check_in_date"]').val().trim(),
                    check_out_date: form.find(':input[name="check_out_date"]').val().trim(),
                    csrfToken: form.find(':input[name="csrfToken"]').val().trim(),
                }
                $.post('requests/core.php', post_data, function (response) {
                    //console.log(response);
                    if (response.hasOwnProperty('OK') && response.OK) {
                        alert('Success! Your booking is saved!');
                        let search_array_new = [];
                        search_array_new.push('id=' + form.find(':input[name="room_id"]').val().trim());
                        search_array_new.push('check_in_date=' + form.find(':input[name="check_in_date"]').val().trim());
                        search_array_new.push('check_out_date=' + form.find(':input[name="check_out_date"]').val().trim());

                        if (search_array_new.length) {
                            window.location.href = '/room.php?' + search_array_new.join('&');
                        }
                    } else if (response.hasOwnProperty('error') && response.error) {
                        CoreFunctions.handleFormResponseErrors(form, response);
                    } else if (response.hasOwnProperty('auth_error')) {
                        window.location.href = "/logout.php"
                    }
                }, 'json');
            });
        }
    });

    $('.room_page').on('click', '.favorite_button ', function () {
        let _this = $(this);
        let post_data = {
            room_id: $('.booked_container_form').find(':input[name="room_id"]').val().trim(),
            action: 'changeRoomFavoriteStatus'
        }

        if (_this.hasClass('is_favorite')) {
            post_data.favorite = 'no';
        } else {
            post_data.favorite = 'yes';
        }

        $.post('requests/core.php', post_data, function (response) {
            if (response.hasOwnProperty('OK') && response.OK) {
                window.location.reload();
            }
        }, 'json')
    });

    $('form.search_filters_form').on('click', '.find_hotels_button', function () {
        let _this = $(this);
        let form = _this.closest('form');
        let search_array_new = [];

        let count_of_guests = form.find(':input[name="count_of_guests"]').val();
        if (count_of_guests && count_of_guests.trim()) {
            search_array_new.push('count_of_guests=' + count_of_guests.trim());
        }

        let room_type = form.find(':input[name="room_type"]').val();
        if (room_type && room_type.trim()) {
            search_array_new.push('room_type=' + room_type.trim());
        }

        let city = form.find(':input[name="city"]').val();
        if (city && city.trim()) {
            search_array_new.push('city=' + city.trim());
        }

        let check_in_date = form.find(':input[name="check_in_date"]').val();
        if (check_in_date && check_in_date.trim()) {
            search_array_new.push('check_in_date=' + check_in_date.trim());
        }

        let check_out_date = form.find(':input[name="check_out_date"]').val();
        if (check_out_date && check_out_date.trim()) {
            search_array_new.push('check_out_date=' + check_out_date.trim());
        }

        let from_price = form.find(':input[name="from_slider_value"]').val();
        if (from_price && from_price.trim()) {
            search_array_new.push('from_price=' + from_price.trim().replace('€', ''));
        }

        let to_price = form.find(':input[name="to_slider_value"]').val();
        if (to_price && to_price.trim()) {
            search_array_new.push('to_price=' + to_price.trim().replace('€', ''));
        }

        if (search_array_new.length) {
            window.location.href = '/list.php?' + search_array_new.join('&');
        } else {
            alert('Please select at least one filter.');
        }
    });
});