CoreFunctions = (function () {

    return {
        clearErrorsFromForm: function (form) {
            form.find('.error_message_container').html('');
            form.find(':input.error_border').removeClass('error_border');
        },
        generateCSRFToken: function (form) {
            return new Promise(function (resolve, reject) {
                    if (form.find(':input[name="csrfToken"]').length) {
                        $.post('requests/core.php', {
                            generateToken: 'generateToken'
                        }, function (response) {
                            if (response.hasOwnProperty('csrfToken')) {
                                form.find('input[name="csrfToken"]').val(response.csrfToken);
                                resolve();
                            } else {
                                window.location.href = '/';
                            }
                        }, 'json');
                    } else {
                        resolve();
                    }
                }
            );
        },
        handleFormResponseErrors: function (form, response) {
            if (response.hasOwnProperty('error')) {
                let error_messages = [];

                if (response.hasOwnProperty('error_required_inputs') &&
                    response.error_required_inputs.length
                ) {
                    let required_inputs = response.error_required_inputs;
                    for (let i = 0; i < required_inputs.length; i++) {
                        let input_name = required_inputs[i];
                        let input_element = form.find(':input[name="' + input_name + '"]');

                        if (input_element.length) {
                            input_element.addClass('error_border');
                            let input_error_message = input_element.attr('data-required-message');
                            if (input_error_message) {
                                error_messages.push(input_error_message);
                            }
                        }
                    }

                } else if (response.hasOwnProperty('error_validate_regexp_inputs') &&
                    response.error_validate_regexp_inputs.length
                ) {
                    let validate_regexp_inputs = response.error_validate_regexp_inputs;
                    for (let i = 0; i < validate_regexp_inputs.length; i++) {
                        let input_name = validate_regexp_inputs[i];
                        let input_element = form.find(':input[name="' + input_name + '"]');

                        if (input_element.length) {
                            input_element.addClass('error_border');
                            let input_error_message = input_element.attr('data-validate_regexp-message');
                            if (input_error_message) {
                                error_messages.push(input_error_message);
                            }
                        }
                    }
                } else if (response.hasOwnProperty('error_validate_with_field_inputs') &&
                    response.error_validate_with_field_inputs.length
                ) {
                    let validate_with_field_inputs = response.error_validate_with_field_inputs;
                    for (let i = 0; i < validate_with_field_inputs.length; i++) {
                        let input_name = validate_with_field_inputs[i];
                        let input_element = form.find(':input[name="' + input_name + '"]');

                        if (input_element.length) {
                            input_element.addClass('error_border');
                            let second_field_name = input_element.data('validate_with_field');
                            let second_element = form.find(':input[name="' + second_field_name + '"]');
                            second_element.addClass('error_border');
                            let input_error_message = input_element.attr('data-validate_with_field-message');
                            if (input_error_message) {
                                error_messages.push(input_error_message);
                            }
                        }
                    }
                } else if (response.hasOwnProperty('error_message')) {
                    error_messages.push(response.error_message);
                }

                if (error_messages.length) {
                    form.find('.error_message_container').html(error_messages.join('<br>'));
                }
            }
        },
        validateForm: function (form) {
            let form_valid = true;
            let error_messages = [];

            CoreFunctions.clearErrorsFromForm(form);

            form.find(':input[type!="hidden"][required]').each(function () {
                let _this = $(this);
                if (_this.val() === null || !_this.val().trim()) {
                    form_valid = false;
                    _this.addClass('error_border');

                    let input_error_message = _this.attr('data-required-message');
                    if (input_error_message) {
                        error_messages.push(input_error_message);
                    }
                }
            });

            form.find(':input[type!="hidden"][data-validate_regexp]').each(function () {
                let _this = $(this);
                let value = _this.val().trim();
                let regexp = _this.data('validate_regexp');
                if (value && regexp) {
                    if (!(new RegExp(regexp).test(value))) {
                        form_valid = false;
                        _this.addClass('error_border');

                        let input_error_message = _this.attr('data-validate_regexp-message');
                        if (input_error_message) {
                            error_messages.push(input_error_message);
                        }
                    }
                }
            });

            form.find(':input[type!="hidden"][data-validate_with_field]').each(function () {
                let _this = $(this);
                let value = _this.val().trim();
                let second_field_name = _this.data('validate_with_field');
                let second_element = form.find(':input[name="' + second_field_name + '"]');
                if (value && second_element.length) {
                    let second_element_value = second_element.val().trim();
                    if (value !== second_element_value) {
                        form_valid = false;
                        _this.addClass('error_border');
                        second_element.addClass('error_border');

                        let input_error_message = _this.attr('data-validate_with_field-message');
                        if (input_error_message) {
                            error_messages.push(input_error_message);
                        }
                    }
                }
            });

            if (error_messages.length) {
                form.find('.error_message_container').html(error_messages.join('<br>'));
            }
            return form_valid;
        }
    }
}());