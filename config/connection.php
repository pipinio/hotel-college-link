<?php
require_once __DIR__ . "/config.php";
require_once __DIR__ . "/autoload.php";

$connli = @new ConnMysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

if ($connli->connect_errno) {
    //die('Connect Error: ' . $connli->connect_errno);
    printf("Cannot connect at this time, maybe server is too busy. Please try again by hitting F5. If the problem persists please contact the Administrator at info@iproject.io We are sorry for any inconvenience.");
    exit();
}

$connli->set_charset("utf8");

header('Cache-Control: must-revalidate');

header('X-Content-Type-Options: nosniff');
header('X-XSS-Protection: 1; mode=block');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}