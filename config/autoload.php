<?php

class AutoLoader
{
    public static function load($class)
    {
        $file_base = realpath(dirname(__FILE__)) . '/../classes/';
        if (strpos($class, 'Traits') !== false) {
            $file_base .= 'Traits/';
        }

        $file = $file_base . $class . '.php';
        if (file_exists($file)) {
            include_once $file;
        }
        else {
            header('HTTP/1.0 404');
            //echo 'Class ' . $file . ' not found';
            exit();
        }
    }
}

/**
 * Function to load dynamically classes
 */
spl_autoload_register(array('AutoLoader', 'load'));
