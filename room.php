<?php
require_once __DIR__ . "/includes/_session.php";
$room_id = filter_var($_GET['id'], FILTER_VALIDATE_INT);

$check_in_date = filter_var($_GET['check_in_date'], FILTER_SANITIZE_STRING);
$check_out_date = filter_var($_GET['check_out_date'], FILTER_SANITIZE_STRING);

$room = $core->getRoom($room_id, $check_in_date, $check_out_date);

//print '<pre>' . print_r($room) . '</pre>';
if (!$room) {
    header('Location: /');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hotels - Πληροφορίες Δωματίου</title>
    <meta name="description" content="Hotel College link project">
    <meta name="author" content="Despina Litsa">
    <?php
    include_once __DIR__ . '/includes/_css.php';
    ?>
</head>
<body>
<?php
include_once __DIR__ . '/includes/_header.php';
?>
<div class="wrapper">
    <div class="container room_page">
        <div class="room_page_title_container">
            <div class="room_basic_info">
                <div class="info_item"><?= $room['name'] ?> - <?= $room['city'] ?>, <?= $room['area'] ?></div>
                <div class="info_item room_reviews">Reviews: <?= $room['stars_html'] ?></div>
                <?php
                if ($user_id) {
                    ?>
                    <div class="info_item favorite_button <?= ($room['is_favorite'] ? 'is_favorite' : '') ?>"
                         data-favorite="<?= $room['is_favorite'] ? 'yes' : 'no' ?>"><i class="fas fa-heart"></i></div>
                <?php } ?>
            </div>
            <div class="room_price">Per Night: <?= $room['price'] ?>€</div>
        </div>
        <div class="room_image" style="background-image: url('../images/rooms/<?= $room['photo_url'] ?>');"></div>
        <div class="room_more_info">
            <div class="info_item">
                <div class="top_part"><i
                            class="fas fa-user"></i><?= $room['count_of_guests'] ?>
                </div>
                <div class="bottom_part">COUNT OF GUESTS</div>
            </div>
            <div class="info_item">
                <div class="top_part"><i
                            class="fas fa-bed"></i><?= $room['room_type_title'] ?>
                </div>
                <div class="bottom_part">TYPE OF ROOM</div>
            </div>
            <div class="info_item">
                <div class="top_part"><i
                            class="fas fa-parking"></i><?= ($room['parking']) ? 'Yes' : 'No' ?>
                </div>
                <div class="bottom_part">PARKING</div>
            </div>
            <div class="info_item">
                <div class="top_part"><i
                            class="fas fa-wifi"></i><?= ($room['wifi']) ? 'Yes' : 'No' ?>
                </div>
                <div class="bottom_part">WIFI</div>
            </div>
            <div class="info_item">
                <div class="top_part"><?= ($room['pet_friendly']) ? 'Yes' : 'No' ?></div>
                <div class="bottom_part">PET FRIENDLY</div>
            </div>
        </div>
        <div class="room_description_container">
            <div>Room Description</div>
            <div class="room_description"><?= $room['description_long'] ?></div>
        </div>
        <form class="booked_container booked_container_form">
            <input type="text" name="check_in_date" class="datepicker_element" placeholder="Check-in Date"
                   required data-required-message="Check-in date is required for search."
                   value="<?= $check_in_date ?>"/>
            <input type="text" name="check_out_date" class="datepicker_element" placeholder="Check-out Date"
                   required data-required-message="Check-out date is required for search."
                   value="<?= $check_out_date ?>"/>
            <input type='hidden' name='room_id' value="<?= $room_id ?>"/>
            <input type='hidden' name='csrfToken'/>
            <?php
            if ($room['is_booked']) {
                ?>
                <div class="already_booked_tag" style="margin-left: 10px;">Already Booked</div>
            <?php } ?>
            <div class="book_room generic_submit_button">Book Room</div>
            <div class="error_message_container" style="width: 300px;float: right;"></div>

        </form>
        <div class="maps_container">
            <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?= $room['location_lat'] ?>,<?= $room['location_long'] ?>&markers=<?= $room['location_lat'] ?>,<?= $room['location_long'] ?>&zoom=18&size=800x400&key=<?= GOOGLE_API_KEY ?>"/>
        </div>
    </div>
    <div class="container room_page_reviews_container">
        <div class="room_page_reviews">
            <div>Reviews</div>
            <div class="review_container">
                <?php
                if (count($room['reviews'])) {
                    foreach ($room['reviews'] as $review_key => $review) {
                        ?>
                        <div class="review_item">
                            <div class="review_username_container"><span
                                        class="review_username"><?= $review_key + 1 ?>. <?= $review['username'] ?></span>
                                <?= $review['stars_html'] ?>
                            </div>
                            <div class="review_date">Added time: <?= $review['created_time'] ?></div>
                            <div class="review_text"><?= $review['comment'] ?></div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="review_item">No reviews.</div>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
        if ($user_id) {
            ?>
            <form class="room_page_add_review add_review_form">
                <div>Add review</div>
                <div class="add_review_container">
                    <div class="review_stars">
                        <i class="fas fa-star review_star"></i>
                        <i class="fas fa-star review_star"></i>
                        <i class="fas fa-star review_star"></i>
                        <i class="fas fa-star review_star"></i>
                        <i class="fas fa-star review_star"></i>
                    </div>
                    <textarea name="my_review" cols="30" rows="10" placeholder="Review"
                              required data-required-message="Please enter a review."></textarea>
                    <input type='hidden' name='csrfToken'/>
                    <input type='hidden' name='room_id' value="<?= $room_id ?>"/>
                    <div class="error_message_container"></div>
                </div>
                <div class="submit_button_container">
                    <div class="submit_review generic_submit_button">Submit</div>
                </div>
            </form>
        <?php } ?>
    </div>
</div>
<?php
include_once __DIR__ . '/includes/_footer.php';
include_once __DIR__ . '/includes/_js.php';
?>
</body>
</html>