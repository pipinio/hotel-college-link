<?php
require_once __DIR__ . "/includes/_session.php";

$prices = $core->getMinMaxPrices();
$cities = $core->getCities();
$room_types = $core->getRoomTypes();
$count_of_guests_select = $core->getCountOfGuests();

$check_in_date = filter_var($_GET['check_in_date'], FILTER_SANITIZE_STRING);
$check_out_date = filter_var($_GET['check_out_date'], FILTER_SANITIZE_STRING);
$city = filter_var($_GET['city'], FILTER_SANITIZE_STRING);
$room_type = filter_var($_GET['room_type'], FILTER_VALIDATE_INT);
$count_of_guests = filter_var($_GET['count_of_guests'], FILTER_VALIDATE_INT);

$from_price = isset($_GET['from_price']) ? filter_var($_GET['from_price'], FILTER_VALIDATE_INT) : $prices[0];
$to_price = isset($_GET['to_price']) ? filter_var($_GET['to_price'], FILTER_VALIDATE_INT) : $prices[1];

$rooms = $core->getAllRooms($city, $room_type, $count_of_guests, $from_price, $to_price, $check_in_date, $check_out_date);

//print '<pre>' . print_r($rooms) . '</pre>';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hotels - Αποτελέσματα Αναζήτησης</title>
    <meta name="description" content="Hotel College link project">
    <meta name="author" content="Despina Litsa">
    <?php
    include_once __DIR__ . '/includes/_css.php';
    ?>
</head>
<body>
<?php
include_once __DIR__ . '/includes/_header.php';
?>
<div class="wrapper">
    <div class="container container_with_sidebar search_page_container">
        <div class="sidebar filters_sidebar">
            <div class="title text-center">FIND THE PERFECT HOTEL</div>
            <form class="filters_container search_filters_form">
                <select name="count_of_guests" class="search_filter_count_of_guests custom-select">
                    <option value="" disabled <?= (!$count_of_guests) ? 'selected' : '' ?>>Count of Guests</option>
                    <?php
                    if (count($count_of_guests_select)) {
                        foreach ($count_of_guests_select as $guest_number) {
                            ?>
                            <option value="<?= $guest_number ?>" <?= ($count_of_guests && $count_of_guests == $guest_number) ? 'selected' : '' ?>><?= $guest_number ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <select name="room_type" class="search_filter_room_type custom-select">
                    <option value="" disabled <?= (!$room_type) ? 'selected' : '' ?>>Room Type</option>
                    <?php
                    if (count($room_types)) {
                        foreach ($room_types as $room_type) {
                            ?>
                            <option value="<?= $room_type['type_id'] ?>" <?= ($room_type && $room_type == $room_type['type_id']) ? 'selected' : '' ?>><?= $room_type['title'] ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <select name="city" class="search_filter_city custom-select">
                    <option value="" disabled <?= (!$city) ? 'selected' : '' ?>>City</option>
                    <?php
                    if (count($cities)) {
                        foreach ($cities as $city_option) {
                            ?>
                            <option value="<?= $city_option ?>" <?= ($city && $city == $city_option) ? 'selected' : '' ?>><?= $city_option ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <div class="range_slider_inputs_container" data-slider_element="search_prices_range_slider">
                    <input type="text" name="from_slider_value" class="range_slider_inputs" data-index="0"
                           value="<?= $from_price ?>€" disabled/>
                    <input type="text" name="to_slider_value" class="range_slider_inputs" data-index="1"
                           value="<?= $to_price ?>€" disabled/>
                </div>
                <div class="range_slider_element" id="search_prices_range_slider"
                     data-min_value="<?= $prices[0] ?>" data-max_value="<?= $prices[1] ?>"
                     data-selected_min_value="<?= $from_price ?>" data-selected_max_value="<?= $to_price ?>"></div>
                <div class="range_slider_price_tags">
                    <span>PRICE MIN.</span>
                    <span>PRICE MAX.</span>
                </div>
                <input type="text" name="check_in_date" class="datepicker_element" placeholder="Check-in Date"
                       value="<?= $check_in_date ?>">
                <input type="text" name="check_out_date" class="datepicker_element" placeholder="Check-out Date"
                       value="<?= $check_out_date ?>">
                <div class="find_hotels_button text-center">FIND HOTEL</div>
            </form>
        </div>
        <div class="list_container search_results_container">
            <div class="search_results_title">Search Results</div>
            <div class="column_list search_results_list">
                <?php
                if (count($rooms)) {
                    foreach ($rooms as $room) {
                        ?>
                        <div class="row_item search_result_item">
                            <div class="left_part">
                                <div class="item_photo"
                                     style="background-image: url('../images/rooms/<?= $room['photo_url'] ?>');">
                                </div>
                                <div class="price_container price_per_night">Per night: <?= $room['price'] ?>$</div>
                            </div>
                            <div class="right_part">
                                <div class="item_info">
                                    <div class="info_parts hotel_title"><?= $room['name'] ?></div>
                                    <div class="info_parts hotel_location"><?= $room['city'] ?>
                                        , <?= $room['area'] ?></div>
                                    <div class="info_parts hotel_description"><?= $room['description_short'] ?></div>
                                    <div class="hotel_button_link"><a href="/room.php?id=<?= $room['room_id'] ?>">Go to
                                            Room Page</a></div>
                                </div>
                                <div class="all_booking_details">
                                    <div class="all_booking_detail_item">Count of
                                        Guests: <?= $room['count_of_guests'] ?></div>
                                    <div class="all_booking_detail_item">Type of
                                        Room: <?= $room['room_type_title'] ?></div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                } else {
                    print 'No rooms found.';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
include_once __DIR__ . '/includes/_footer.php';
include_once __DIR__ . '/includes/_js.php';
?>
</body>
</html>