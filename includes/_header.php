<?php
$hotels_active = $profile_active = $home_active = $login_active = false;
$simple_header = false;

if (strpos($active_file, '/index.php') !== false) {
    $home_active = true;
    $simple_header = true;
} else if (strpos($active_file, '/profile.php') !== false) {
    $profile_active = true;
} else if (strpos($active_file, '/list.php') !== false) {
    $hotels_active = true;
} else if (strpos($active_file, '/login.php') !== false || strpos($active_file, '/register.php') !== false) {
    $login_active = true;
    $simple_header = true;
}

?>
<header class="<?= $simple_header ? 'light-grey-background' : '' ?>">
    <div class="container header_buttons_container <?= $simple_header ? '' : 'bordered-shadowed-header' ?>">
        <div class="left_buttons_container">
            <div class="header_button text-center hotels_button">
                <a class="gray-color <?= $hotels_active ? 'active-option' : '' ?>" href="/list.php">Hotels</a>
            </div>
        </div>
        <div class="right_buttons_container">
            <?php
            if (!$user_id) {
                ?>
                <div class="header_button text-center login_button border-left-dotted">
                    <a class="gray-color <?= $login_active ? 'active-option' : '' ?>" href="/login.php"><i
                                class="fas fa-sign-in-alt"></i>Login</a>
                </div>
                <?php
            } else {
                ?>
                <div class="header_button text-center login_button border-left-dotted">
                    <a class="gray-color <?= $login_active ? 'active-option' : '' ?>" href="/logout.php"><i
                                class="fas fa-sign-out-alt"></i>Logout</a>
                </div>
                <div class="header_button text-center profile_button border-left-dotted">
                    <a class="gray-color <?= $profile_active ? 'active-option' : '' ?>" href="/profile.php"><i
                                class="fas fa-user"></i>Profile</a>
                </div>
            <?php } ?>
            <div class="header_button text-center home_button">
                <a class="gray-color <?= $home_active ? 'active-option' : '' ?>" href="/"><i
                            class="fas fa-home"></i>Home</a>
            </div>
        </div>
    </div>
</header>