<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL && ~E_NOTICE);

require_once __DIR__ . '/../config/connection.php';

$user_id = $_SESSION['user_id'];
$active_file = $_SERVER['SCRIPT_NAME'];

if ($user_id) {
    if ($active_file == '/login.php' || $active_file == '/register.php') {
        header('Location: /');
        exit();
    }
} else {
    if ($active_file == '/profile.php') {
        header('Location: /login.php');
        exit();
    }
}

$core = new Core();