<?php
require_once __DIR__ . "/includes/_session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hotels - Εγγραφή Χρήστη</title>
    <meta name="description" content="Hotel College link project">
    <meta name="author" content="Despina Litsa">
    <?php
    include_once __DIR__ . '/includes/_css.php';
    ?>
</head>
<body>
<?php
include_once __DIR__ . '/includes/_header.php';
?>
<div class="wrapper">
    <div class="register_wrapper_with_bg">
        <form class="register_box register_form">
            <input type="text" name="register_name" placeholder="Name"
                   required data-error-message="Name is required for register."/>
            <input type="email" name="register_email" placeholder="Email"
                   required data-required-message="Email is required for login."
                   data-validate_regexp="^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$"
                   data-validate_regexp-message="Wrong email format."
                   data-validate_with_field="register_email_retype"
                   data-validate_with_field-message="Please enter the same email on both fields."/>
            <input type="email" name="register_email_retype" placeholder="Re-type Email"
                   required data-required-message="Email is required for login."
                   data-validate_regexp="^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$"
                   data-validate_regexp-message="Wrong email format."/>
            <input type="password" name="register_password" placeholder="Password"
                   required data-required-message="Password is required for login."/>
            <input type='hidden' name='csrfToken'/>
            <div class="error_message_container"></div>
            <div class="simple_button register_button_form generic_submit_button">Register</div>
        </form>
    </div>
</div>
<?php
include_once __DIR__ . '/includes/_footer.php';
include_once __DIR__ . '/includes/_js.php';
?>
</body>
</html>