<?php
require_once __DIR__ . "/includes/_session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hotels - Σύνδεση Χρήστη</title>
    <meta name="description" content="Hotel College link project">
    <meta name="author" content="Despina Litsa">
    <?php
    include_once __DIR__ . '/includes/_css.php';
    ?>
</head>
<body>
<?php
include_once __DIR__ . '/includes/_header.php';
?>
<div class="wrapper">
    <div class="login_wrapper_with_bg">
        <form class="login_box login_form">
            <input type="email" name="login_email" placeholder="Email"
                   required data-required-message="Email is required for login."
                   data-validate_regexp="^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$"
                   data-validate_regexp-message="Wrong email format."/>
            <input type="password" name="login_password" placeholder="Password"
                   required data-required-message="Password is required for login."/>
            <input type='hidden' name='csrfToken'/>
            <div class="error_message_container"></div>
            <div class="simple_button login_button_form generic_submit_button">Login</div>
            <div class="register_text"><a href="/register.php">Register here!</a></div>
        </form>
    </div>
</div>
<?php
include_once __DIR__ . '/includes/_footer.php';
include_once __DIR__ . '/includes/_js.php';
?>
</body>
</html>