<?php

class Core extends ConnMysqli
{
    protected $connli = false;
    protected $user_id = false;

    public function __construct()
    {

        $this->connli = @new ConnMysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        if ($this->connli->connect_errno) {
            printf("Cannot connect at this time, maybe server is too busy. Please try again by hitting F5. If the problem persists please contact the Administrator at info@iproject.io We are sorry for any inconvenience.");
            exit();
        }

        $this->connli->set_charset("utf8");

        $this->user_id = $_SESSION['user_id'];
    }

    public static function generateCSRFToken()
    {
        $randomtoken = base64_encode(openssl_random_pseudo_bytes(32));

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['csrfToken'] = $randomtoken;
        session_write_close();
        session_start();

        return $randomtoken;
    }

    public static function getCSRFToken()
    {
        $csrfToken = $_SESSION['csrfToken'];
        return $csrfToken;
    }

    public static function resetCSRFToken()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['csrfToken'] = false;
        session_write_close();
        session_start();
    }

    public static function dateDiffInDays($date1, $date2)
    {
        // Calculating the difference in timestamps
        $diff = strtotime($date2) - strtotime($date1);

        // 1 day = 24 hours
        // 24 * 60 * 60 = 86400 seconds
        return abs(round($diff / 86400));
    }

    public function getCities()
    {
        $data = [];
        $select_stmt = $this->connli->prepare("SELECT DISTINCT(city) as city FROM room");
        if ($select_stmt) {
            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($select_record = $select_result->fetch_assoc()) {
                        $data[] = $select_record['city'];
                    }
                }
            }

            $select_stmt->close();
        }

        return $data;
    }

    public function getCountOfGuests()
    {
        $data = [];
        $select_stmt = $this->connli->prepare("SELECT DISTINCT(count_of_guests) as count_of_guests FROM room");
        if ($select_stmt) {
            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($select_record = $select_result->fetch_assoc()) {
                        $data[] = $select_record['count_of_guests'];
                    }
                }
            }

            $select_stmt->close();
        }

        return $data;
    }

    public function getRoomTypes()
    {
        $data = [];
        $select_stmt = $this->connli->prepare("SELECT type_id,title FROM room_type");
        if ($select_stmt) {
            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($select_record = $select_result->fetch_assoc()) {
                        $data[] = $select_record;
                    }
                }
            }

            $select_stmt->close();
        }

        return $data;
    }

    public function getRoom($id, $check_in_date = false, $check_out_date = false)
    {
        $query = "SELECT r.*, rt.title as room_type_title FROM room r ";
        $query .= "INNER JOIN room_type rt ON rt.type_id = r.type_id ";
        $query .= "WHERE room_id = ? ";

        $data = false;
        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {
            $select_stmt->bind_param('i', $id);
            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    $room_record = $select_result->fetch_assoc();

                    $stars_html = '';
                    $reviews = floor($room_record['avg_reviews']);
                    for ($i = 0; $i < 5; $i++) {
                        $stars_html .= '<i class="fas fa-star ' . ($reviews > $i ? 'active-star' : '') . '"></i>';
                    }
                    $room_record['stars_html'] = $stars_html;

                    $room_record['is_favorite'] = $this->checkIfRoomIsFavorite($id);

                    $room_record['reviews'] = $this->getRoomReviews($id);

                    $room_record['is_booked'] = false;

                    if ($check_in_date && $check_out_date) {
                        $room_record['is_booked'] = $this->checkIfRoomIsBooked($id, $check_in_date, $check_out_date);
                    }

                    $data = $room_record;
                }
            }

            $select_stmt->close();
        }

        return $data;
    }

    public function checkIfRoomIsFavorite($room_id)
    {
        global $user_id;

        $is_favorite = false;
        $select_stmt = $this->connli->prepare("SELECT * FROM favorite WHERE room_id = ? AND user_id = ?");
        if ($select_stmt) {
            $select_stmt->bind_param('ii', $room_id, $user_id);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    $is_favorite = true;
                }
            }

            $select_stmt->close();
        }
        return $is_favorite;
    }

    public function getRoomReviews($room_id)
    {
        $reviews = [];
        $query = "SELECT u.name as username, r.rate, r.comment, r.created_time FROM review r ";
        $query .= "INNER JOIN user u ON r.user_id = u.user_id ";
        $query .= "WHERE r.room_id = ? ORDER BY created_time DESC";

        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {
            $select_stmt->bind_param('i', $room_id);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($review = $select_result->fetch_assoc()) {
                        $stars_html = '';
                        $reviews_stars = floor($review['rate']);
                        for ($i = 0; $i < 5; $i++) {
                            $stars_html .= '<i class="fas fa-star ' . ($reviews_stars > $i ? 'active-star' : '') . '"></i>';
                        }
                        $review['stars_html'] = $stars_html;
                        $reviews[] = $review;
                    }
                }
            }

            $select_stmt->close();
        }

        return $reviews;
    }

    public function addReview($post_data)
    {
        global $user_id;
        $response = [];

        $review = trim(filter_var($post_data['my_review'], FILTER_SANITIZE_STRING));
        $stars = trim(filter_var($post_data['stars'], FILTER_SANITIZE_STRING));
        $room_id = trim(filter_var($post_data['room_id'], FILTER_VALIDATE_INT));

        if ($review && $room_id) {
            $insert_review_stmt = $this->connli->prepare("INSERT INTO review (room_id, user_id, rate, comment) VALUES (?,?,?,?)");
            $insert_review_stmt->bind_param('iiss', $room_id, $user_id, $stars, $review);
            $insert_review_stmt->execute();
            $insert_review_stmt->close();

            $count_reviews = 0;
            $avg_reviews = 0;

            $review_stmt = $this->connli->prepare("SELECT SUM(rate) as sum_reviews, COUNT(*) as count_reviews FROM review WHERE room_id = ?");
            if ($review_stmt) {
                $review_stmt->bind_param('i', $room_id);
                if ($review_stmt->execute()) {
                    $review_result = $review_stmt->get_result()->fetch_assoc();
                    if ($review_result['sum_reviews'] && $review_result['count_reviews']) {
                        $count_reviews = $review_result['count_reviews'];
                        $avg_reviews = $review_result['sum_reviews'] / $review_result['count_reviews'];
                    }
                }
            }

            $update_review_counter_stmt = $this->connli->prepare("UPDATE room SET count_reviews = ?, avg_reviews = ? WHERE room_id = ? ");
            $update_review_counter_stmt->bind_param('sss', $count_reviews, $avg_reviews, $room_id);
            $update_review_counter_stmt->execute();
            $update_review_counter_stmt->close();

            $response['OK'] = true;

        } else {
            $response['error'] = true;
            if (!$review) {
                $response['error_required_inputs'][] = 'my_review';
            }

            if (!$room_id) {
                $response['error_message'] = 'Missing room id.';
            }
        }
        return $response;
    }

    public function checkIfRoomIsBooked($id, $check_in_date = false, $check_out_date = false)
    {
        $is_booked = false;

        $bind_values = [$id];
        $query = "SELECT * FROM booking WHERE room_id = ? ";

        if ($check_in_date) {
            if ($check_out_date) {
                $bind_values[] = $check_in_date;
                $bind_values[] = $check_out_date;
                $bind_values[] = $check_in_date;
                $bind_values[] = $check_out_date;
                $query .= " AND ((check_in_date BETWEEN ? AND ?) OR (check_out_date BETWEEN ? AND ?))";
            } else {
                $bind_values[] = $check_in_date;
                $bind_values[] = $check_in_date;
                $query .= " AND (check_in_date <= ? AND check_out_date >= ?) ";

            }
        } else {
            if ($check_out_date) {
                $bind_values[] = $check_out_date;
                $bind_values[] = $check_out_date;
                $query .= " AND (check_in_date <= ? AND check_out_date >= ?) ";
            }
        }

        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {
            $select_stmt->bind_param(str_repeat('s', count($bind_values)), ...$bind_values);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    $is_booked = true;
                }
            }

            $select_stmt->close();
        }
        return $is_booked;
    }

    public function bookRoom($post_data)
    {
        global $user_id;
        $response = [];

        $check_in_date = trim(filter_var($post_data['check_in_date'], FILTER_SANITIZE_STRING));
        $check_out_date = trim(filter_var($post_data['check_out_date'], FILTER_SANITIZE_STRING));
        $room_id = trim(filter_var($post_data['room_id'], FILTER_VALIDATE_INT));

        if ($check_in_date && $check_out_date && $room_id) {

            $room_is_booked = $this->checkIfRoomIsBooked($room_id, $check_in_date, $check_out_date);
            if ($room_is_booked) {
                $response['error'] = true;
                $response['error_message'] = 'We are sorry but the room is booked for those dates.';
            } else {

                $room = $this->getRoom($room_id);

                if ($room) {
                    $total_price = 0;
                    $price_per_night = $room['price'];
                    $days = self::dateDiffInDays($check_in_date, $check_out_date);

                    if ($days) {
                        $total_price = $days * $price_per_night;
                    }

                    $insert_query = "INSERT INTO booking (user_id, room_id , check_in_date, check_out_date, total_price) VALUES (?,?,?,?,?)";
                    $insert_stmt = $this->connli->prepare($insert_query);
                    $insert_stmt->bind_param('iissi', $user_id, $room_id, $check_in_date, $check_out_date, $total_price);
                    $insert_stmt->execute();
                    $insert_stmt->close();
                    $response['OK'] = true;
                } else {
                    $response['error'] = true;
                    $response['error_message'] = 'Room not found';
                }
            }


        } else {
            $response['error'] = true;
            if (!$check_in_date) {
                $response['error_required_inputs'][] = 'check_in_date';
            }

            if (!$check_out_date) {
                $response['error_required_inputs'][] = 'check_out_date';
            }

            if (!$room_id) {
                $response['error_message'] = 'Missing room id.';
            }
        }
        return $response;
    }

    public function changeRoomFavoriteStatus($post_data)
    {
        global $user_id;
        $room_id = trim(filter_var($post_data['room_id'], FILTER_VALIDATE_INT));
        $favorite = trim(filter_var($post_data['favorite'], FILTER_SANITIZE_STRING));

        if ($room_id && $favorite) {
            if ($favorite == 'yes') {
                $insert_query = "INSERT INTO favorite (user_id, room_id) VALUES (?,?)";
                $insert_stmt = $this->connli->prepare($insert_query);
                $insert_stmt->bind_param('ii', $user_id, $room_id);
                $insert_stmt->execute();
                $insert_stmt->close();
            } else if ($favorite == 'no') {
                $delete_query = "DELETE FROM favorite WHERE user_id = ?  AND room_id = ?";
                $delete_stmt = $this->connli->prepare($delete_query);
                $delete_stmt->bind_param('ii', $user_id, $room_id);
                $delete_stmt->execute();
                $delete_stmt->close();
            }
        }

        $response['OK'] = true;
        return $response;
    }

    public function getMinMaxPrices()
    {
        $prices = [0, 0];
        $select_stmt = $this->connli->prepare("SELECT MIN(price) as min_price, MAX(price) as max_price FROM room ");
        if ($select_stmt) {
            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    $select_record = $select_result->fetch_assoc();

                    if ($select_record['min_price']) {
                        $prices[0] = $select_record['min_price'];
                    }
                    if ($select_record['max_price']) {
                        $prices[1] = $select_record['max_price'];
                    }
                }
            }

            $select_stmt->close();
        }
        return $prices;
    }

    public function getAllRooms($city = false, $room_type = false, $count_of_guests = false, $from_price = false, $to_price = false, $check_in_date = false, $check_out_date = false)
    {
        $rooms = [];

        $query = "SELECT r.room_id, r.photo_url, r.price, r.count_of_guests, r.description_short, r.name, r.city, r.area, rt.title as room_type_title ";
        $query .= "FROM room r ";
        $query .= "INNER JOIN room_type rt ON rt.type_id = r.type_id ";
        $query .= "WHERE (1) ";

        $bind_values = [];

        if ($city) {
            $query .= "AND r.city = ? ";
            $bind_values[] = $city;
        }

        if ($room_type) {
            $query .= "AND r.type_id = ? ";
            $bind_values[] = $room_type;
        }

        if ($count_of_guests) {
            $query .= "AND r.count_of_guests = ? ";
            $bind_values[] = $count_of_guests;
        }

        if ($from_price) {
            $query .= "AND r.price >= ? ";
            $bind_values[] = $from_price;
        }

        if ($to_price) {
            $query .= "AND r.price <= ? ";
            $bind_values[] = $to_price;
        }

        $query .= " ORDER BY r.updated_time DESC";
        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {

            if (count($bind_values)) {
                $select_stmt->bind_param(str_repeat('s', count($bind_values)), ...$bind_values);
            }
            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($select_record = $select_result->fetch_assoc()) {
                        if ($check_in_date || $check_out_date) {
                            $is_booked = $this->checkIfRoomIsBooked($select_record['room_id'], $check_in_date, $check_out_date);

                            if (!$is_booked) {
                                $rooms[] = $select_record;
                            }
                        } else {
                            $rooms[] = $select_record;
                        }
                    }
                }
            }

            $select_stmt->close();
        }

        return $rooms;
    }
}