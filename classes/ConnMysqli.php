<?php
class ConnMysqli extends mysqli
{
//prepare query for multiple use unless params given then executed once and closes statement
    public function prepareQuery($query, $types, $params = null)
    {
        $stmt = false;
        if ($types && $stmt = $this->prepare($query)) {
            $stmt->types = $types;
            if (!is_null($params)) {//single use of prepare & close statement
                $results = $this->execParamsResult($stmt, $params);
                $stmt->close();
                $stmt = $results;
            }
        }
        return $stmt;
    }

    public function executeQuery($query, $types, $params)
    {
        $stmt = false;
        if ($types && $params && $stmt = $this->prepare($query)) {
            $stmt->types = $types;
            $this->execParams($stmt, $params);
//$stmt->close();
        }
        return $stmt;
    }

//Execute statement for multiple use like (Insert, Update, Delete)
    public function execParams($stmt, $params)
    {
        $a_params[] = &$stmt->types;
        if (is_array($params)) {
            foreach ($params as $i => $f) {
                $a_params[] = &$params[$i];
            }
        } else {
            $a_params[] = &$params;
        }
        if ($stmt->param_count != count($a_params) - 1) {
            error_log(date('Y-m-d H:i:s') . " - bind_param Error:" . print_r(debug_backtrace()[0], true) . print_r($a_params, true));
        }
        call_user_func_array([$stmt, 'bind_param'], $a_params);
        $stmt->execute();
        return $stmt;//affected_rows -1:error, 0:no (insert|update|delete), 0>#:affected_rows
    }

//Execute statement for multiple use for Selects with get results
    public function execParamsResult($stmt, $params)
    {
        $stmt = $this->execParams($stmt, $params);
        $result = $stmt->get_result();
        return $result;
    }
}