<?php

class User extends Core
{
    public function loginUser($post_data)
    {
        $response = [];

        $username = trim(filter_var($post_data['login_email'], FILTER_SANITIZE_STRING));
        $password = trim(filter_var($post_data['login_password'], FILTER_SANITIZE_STRING));

        if ($username && $password) {
            /**
             * check username validity
             */
            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $response['error'] = true;
                $response['error_validate_regexp_inputs'][] = 'login_email';
            } else {
                $password_sha1 = sha1($password);

                //check if user exists on db
                $user_exists = $this->checkIfUserExists($username, $password_sha1);
                if ($user_exists) {

                    if (session_status() == PHP_SESSION_NONE) {
                        session_start();
                    }
                    $_SESSION['user_id'] = $user_exists['user_id'];
                    session_write_close();
                    session_start();

                    $response['OK'] = true;

                } else {
                    $response['error'] = true;
                    $response['error_message'] = 'Wrong email and/or password.';
                }
            }
        } else {
            $response['error'] = true;
            if (!$username) {
                $response['error_required_inputs'][] = 'login_email';
            }
            if (!$password) {
                $response['error_required_inputs'][] = 'login_password';
            }
        }
        return $response;
    }

    public function registerUser($post_data)
    {
        $response = [];

        $name = trim(filter_var($post_data['register_name'], FILTER_SANITIZE_STRING));
        $email = trim(filter_var($post_data['register_email'], FILTER_SANITIZE_STRING));
        $email_retype = trim(filter_var($post_data['register_email_retype'], FILTER_SANITIZE_STRING));
        $password = trim(filter_var($post_data['register_password'], FILTER_SANITIZE_STRING));

        if ($name && $email && $email_retype && $password) {
            /**
             * check email validity
             */
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $response['error'] = true;
                $response['error_validate_regexp_inputs'][] = 'register_email';
            } else if (!filter_var($email_retype, FILTER_VALIDATE_EMAIL)) {
                $response['error'] = true;
                $response['error_validate_regexp_inputs'][] = 'register_email_retype';
            } else {
                /**
                 * check if the two emails are the same
                 */
                if ($email === $email_retype) {

                    /**
                     * check if email exists on db
                     */
                    $user_exists = $this->checkIfEmailExists($email);
                    if ($user_exists) {
                        $response['error'] = true;
                        $response['error_message'] = 'Email already in use.';
                    } else {
                        $password_sha1 = sha1($password);
                        $insert_query = "INSERT INTO user (name,email,password) VALUES (?,?,?)";
                        $insert_stmt = $this->connli->prepare($insert_query);
                        if ($insert_stmt) {
                            $insert_stmt->bind_param('sss', $name, $email, $password_sha1);
                            $insert_stmt->execute();
                            $response['OK'] = true;
                        } else {
                            $response = ['auth_error' => 'No_access'];
                        }
                    }
                } else {
                    $response['error'] = true;
                    $response['error_validate_with_field_inputs'][] = 'register_email';
                }
            }
        } else {
            $response['error'] = true;
            if (!$name) {
                $response['error_required_inputs'][] = 'register_name';
            }
            if (!$email) {
                $response['error_required_inputs'][] = 'register_email';
            }
            if (!$email_retype) {
                $response['error_required_inputs'][] = 'register_email_retype';
            }
            if (!$password) {
                $response['error_required_inputs'][] = 'register_password';
            }
        }

        return $response;
    }

    public function checkIfUserExists($username, $password)
    {
        $user_exists = false;
        $select_stmt = $this->connli->prepare("SELECT user_id FROM user WHERE email = ? AND password = ?");
        if ($select_stmt) {
            $select_stmt->bind_param('ss', $username, $password);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    $user_exists = $select_result->fetch_assoc();
                }
            }

            $select_stmt->close();
        }

        return $user_exists;
    }

    public function checkIfEmailExists($email)
    {
        $user_exists = false;
        $select_stmt = $this->connli->prepare("SELECT user_id FROM user WHERE email = ?");
        if ($select_stmt) {
            $select_stmt->bind_param('s', $email);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    $user_exists = $select_result->fetch_assoc();
                }
            }

            $select_stmt->close();
        }

        return $user_exists;
    }

    public function getUserDetails()
    {
        return [
            'favorites' => $this->getUserFavorites(),
            'reviews' => $this->getUserReviews(),
            'bookings' => $this->getUserBookings()
        ];
    }

    public function getUserReviews()
    {
        global $user_id;
        $reviews = [];

        $query = "SELECT r.rate, room.name FROM review r ";
        $query .= "INNER JOIN room ON room.room_id = r.room_id ";
        $query .= "WHERE r.user_id = ? ORDER BY r.updated_time DESC";

        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {
            $select_stmt->bind_param('i', $user_id);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($review = $select_result->fetch_assoc()) {
                        $stars_html = '';
                        $reviews_stars = floor($review['rate']);
                        for ($i = 0; $i < 5; $i++) {
                            $stars_html .= '<i class="fas fa-star ' . ($reviews_stars > $i ? 'active-star' : '') . '"></i>';
                        }
                        $review['stars_html'] = $stars_html;
                        $reviews[] = $review;
                    }
                }
            }

            $select_stmt->close();
        }

        return $reviews;
    }

    public function getUserFavorites()
    {
        global $user_id;
        $favorites = [];

        $query = "SELECT room.name FROM favorite r ";
        $query .= "INNER JOIN room ON room.room_id = r.room_id ";
        $query .= "WHERE r.user_id = ? ORDER BY r.updated_time DESC";

        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {
            $select_stmt->bind_param('i', $user_id);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($favorite = $select_result->fetch_assoc()) {
                        $favorites[] = $favorite;
                    }
                }
            }

            $select_stmt->close();
        }

        return $favorites;
    }

    public function getUserBookings()
    {
        global $user_id;
        $bookings = [];

        $query = "SELECT b.check_in_date, b.check_out_date, b.total_price, room.name, room.description_short, ";
        $query .= "b.room_id, room.city, room.area, room.photo_url, room_type.title as room_type_title ";
        $query .= "FROM booking b ";
        $query .= "INNER JOIN room ON room.room_id = b.room_id ";
        $query .= "INNER JOIN room_type ON room.type_id = room_type.type_id ";
        $query .= "WHERE b.user_id = ? ORDER BY b.updated_time DESC";

        $select_stmt = $this->connli->prepare($query);
        if ($select_stmt) {
            $select_stmt->bind_param('i', $user_id);

            if ($select_stmt->execute()) {
                $select_result = $select_stmt->get_result();

                if ($select_result->num_rows && $select_result->num_rows > 0) {
                    while ($booking = $select_result->fetch_assoc()) {
                        $bookings[] = $booking;
                    }
                }
            }

            $select_stmt->close();
        }

        return $bookings;
    }

}