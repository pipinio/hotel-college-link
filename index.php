<?php
require_once __DIR__ . "/includes/_session.php";

$cities = $core->getCities();
$room_types = $core->getRoomTypes();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Hotels - Αναζήτηση δωματίου</title>
    <meta name="description" content="Hotel College link project">
    <meta name="author" content="Despina Litsa">
    <?php
    include_once __DIR__ . '/includes/_css.php';
    ?>
</head>
<body>
<?php
include_once __DIR__ . '/includes/_header.php';
?>
<div class="wrapper">
    <div class="wrapper_with_bg">
        <form class="search_box search_index_form">
            <div class="dropdowns_container">
                <select name="city" class="custom-select"
                        required data-required-message="City is required for search.">
                    <option value="" disabled selected>City</option>
                    <?php
                    if (count($cities)) {
                        foreach ($cities as $city) {
                            ?>
                            <option value="<?= $city ?>"><?= $city ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <select name="room_type" class="custom-select">
                    <option value="" disabled selected>Room Type</option>
                    <?php
                    if (count($room_types)) {
                        foreach ($room_types as $room_type) {
                            ?>
                            <option value="<?= $room_type['type_id'] ?>"><?= $room_type['title'] ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="dates_container">
                <input type="text" name="check_in_date" class="datepicker_element" placeholder="Check-in Date"
                       required data-required-message="Check-in date is required for search."/>
                <input type="text" name="check_out_date" class="datepicker_element" placeholder="Check-out Date"
                       required data-required-message="Check-out date is required for search."/>
            </div>
            <div class="error_message_container"></div>
            <div class="search_button_container">
                <div class="search_button simple_button generic_submit_button">Search</div>
            </div>
        </form>
    </div>
</div>
<?php
include_once __DIR__ . '/includes/_footer.php';
include_once __DIR__ . '/includes/_js.php';
?>
</body>
</html>