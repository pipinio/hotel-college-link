<?php
require_once __DIR__ . "/../includes/_session.php";

header('Content-Type: application/json');

if (isset($_POST['generateToken'])) {
    $randomtoken = Core::generateCSRFToken();
    print json_encode(['csrfToken' => $randomtoken]);
    exit();
}
if (isset($_POST['resetToken'])) {
    Core::resetCSRFToken();
    exit();
}
if (isset($_POST['action'])) {

    $response = [];

    $action = filter_var($_POST['action'], FILTER_SANITIZE_STRING);
    $postCRSFToken = filter_var($_POST['csrfToken'], FILTER_SANITIZE_STRING);

    switch ($action) {
        case 'loginUser':
            $csrfToken = Core::getCSRFToken();

            if ($csrfToken && $postCRSFToken && $csrfToken == $postCRSFToken) {
                $user = new User();
                $response = $user->loginUser($_POST);
                print json_encode($response);
                exit();
            } else {
                Core::resetCSRFToken();
                print json_encode(['auth_error' => 'No_access']);
                exit();
            }
            break;
        case 'registerUser':
            $csrfToken = Core::getCSRFToken();

            if ($csrfToken && $postCRSFToken && $csrfToken == $postCRSFToken) {
                $user = new User();
                $response = $user->registerUser($_POST);
                print json_encode($response);
                exit();
            } else {
                Core::resetCSRFToken();
                print json_encode(['auth_error' => 'No_access']);
                exit();
            }
            break;
        case 'addReview':
            $csrfToken = Core::getCSRFToken();

            if ($csrfToken && $postCRSFToken && $csrfToken == $postCRSFToken) {
                $response = $core->addReview($_POST);
                print json_encode($response);
                exit();
            } else {
                Core::resetCSRFToken();
                print json_encode(['auth_error' => 'No_access']);
                exit();
            }
            break;
        case 'bookRoom':
            $csrfToken = Core::getCSRFToken();

            if ($csrfToken && $postCRSFToken && $csrfToken == $postCRSFToken) {
                $response = $core->bookRoom($_POST);
                print json_encode($response);
                exit();
            } else {
                Core::resetCSRFToken();
                print json_encode(['auth_error' => 'No_access']);
                exit();
            }
            break;
        case 'changeRoomFavoriteStatus':
            $response = $core->changeRoomFavoriteStatus($_POST);
            print json_encode($response);
            exit();
            break;
    }
}